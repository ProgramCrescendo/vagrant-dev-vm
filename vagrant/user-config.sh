#!/usr/bin/env bash

apacheversion='apache-tomcat-9.0.14'

rm -rf ~/apps ~/temp/
mkdir ~/apps ~/temp
wget -P ~/temp/ --progress=bar:force https://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.14/bin/$apacheversion.tar.gz 
tar zxvf ~/temp/$apacheversion.tar.gz -C ~/apps/
cp /vagrant/vagrant/tomcat-config/tomcat-users.xml ~/apps/$apacheversion/conf/
cp /vagrant/vagrant/tomcat-config/server.xml ~/apps/$apacheversion/conf/
mkdir -p ~/apps/$apacheversion/conf/Catalina/localhost
cp /vagrant/vagrant/tomcat-config/manager.xml ~/apps/$apacheversion/conf/Catalina/localhost/manager.xml
~/apps/$apacheversion/bin/startup.sh

# clean
rm -rf ~/temp/