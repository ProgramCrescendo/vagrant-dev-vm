#!/usr/bin/env bash

add-apt-repository ppa:openjdk-r/ppa
apt-get update
apt-get install -y openjdk-8-jdk

su -c "source /vagrant/vagrant/user-config.sh" vagrant